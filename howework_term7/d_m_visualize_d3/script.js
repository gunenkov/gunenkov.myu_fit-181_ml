var margin = {
    top: 20,
    right: 120,
    bottom: 20,
    left: 120,
  },
  width = 960 - margin.right - margin.left,
  height = 800 - margin.top - margin.bottom;

var root = {
  name: 'Дескрипторная модель 2',
  children: [
    {
      name: 'Знать',
      children: [
        {
          name: 'Элементы web-разработки',
          children: [
            {
              name: 'приложение',
              weight: 0.1762,
            },
            {
              name: 'javascript',
              weight: 0.052,
            },
            {
              name: 'typescript',
              weight: 0.0495,
            },
            {
              name: 'тестирование',
              weight: 0.0438,
            },
            {
              name: 'пакет',
              weight: 0.0438,
            },
          ],
        },
        {
          name: 'Язык программирования TypeScript',
          children: [
            {
              name: 'метод',
              weight: 0.1146,
            },
            {
              name: 'значение',
              weight: 0.0893,
            },
            {
              name: 'событие',
              weight: 0.0656,
            },
            {
              name: 'привязка',
              weight: 0.051,
            },
            {
              name: 'возвращать',
              weight: 0.0272,
            },
            {
              name: 'массив',
              weight: 0.0255,
            },
          ],
        },
        {
          name: 'ООП',
          children: [
            {
              name: 'компонент',
              weight: 0.222,
            },
            {
              name: 'класс',
              weight: 0.1217,
            },
            {
              name: 'служба',
              weight: 0.0832,
            },
            {
              name: 'тип',
              weight: 0.078,
            },
            {
              name: 'определение',
              weight: 0.0359,
            },
            {
              name: 'экземпляр',
              weight: 0.0284,
            },
            {
              name: 'декоратор',
              weight: 0.0279,
            },
            {
              name: 'внедрение',
              weight: 0.0247,
            },
            {
              name: 'конструктор',
              weight: 0.0151,
            },
          ],
        },
        {
          name: 'Директивы в Angular',
          children: [
            {
              name: 'ngmodel',
              weight: 0.0377,
            },
            {
              name: 'ngswitch',
              weight: 0.0331,
            },
            {
              name: 'ngfor',
              weight: 0.033,
            },
            {
              name: 'директива',
              weight: 0.0266,
            },
            {
              name: 'ngswitchcase',
              weight: 0.0198,
            },
            {
              name: 'ngswitchdefault',
              weight: 0.018,
            },
            {
              name: 'ngform',
              weight: 0.0177,
            },
            {
              name: 'ngif',
              weight: 0.0173,
            },
          ],
        },
        {
          name: 'Основы разработки BackEnd',
          children: [
            {
              name: 'база',
              weight: 0.0347,
            },
            {
              name: 'клиент',
              weight: 0.0287,
            },
            {
              name: 'клиент',
              weight: 0.0276,
            },
            {
              name: 'ввод',
              weight: 0.0242,
            },
            {
              name: 'реляционный',
              weight: 0.021,
            },

            {
              name: 'протокол',
              weight: 0.0178,
            },
          ],
        },
      ],
    },
    {
      name: 'Уметь',
      children: [
        {
          name: 'Маршрутизация',
          children: [
            {
              name: 'маршрут',
              weight: 0.0599,
            },
            {
              name: 'url',
              weight: 0.0512,
            },
            {
              name: 'запрос',
              weight: 0.0496,
            },
            {
              name: 'http',
              weight: 0.0477,
            },
            {
              name: 'routerlink',
              weight: 0.0452,
            },
            {
              name: 'адрес',
              weight: 0.0396,
            },
          ],
        },
        {
          name: 'Визуализация данных',
          children: [
            {
              name: 'элемент',
              weight: 0.1214,
            },
            {
              name: 'директива',
              weight: 0.0875,
            },
            {
              name: 'шаблон',
              weight: 0.0659,
            },
            {
              name: 'стиль',
              weight: 0.0204,
            },
            {
              name: 'анимация',
              weight: 0.0181,
            },
            {
              name: 'css',
              weight: 0.0149,
            },
            {
              name: 'dom',
              weight: 0.0144,
            },
          ],
        },
        {
          name: 'Сборка приложения',
          children: [
            {
              name: 'файл',
              weight: 0.0184,
            },
            {
              name: 'package',
              weight: 0.0182,
            },
            {
              name: 'папка',
              weight: 0.017,
            },
            {
              name: 'aot',
              weight: 0.132,
            },
            {
              name: 'конфигурация',
              weight: 0.0112,
            },
          ],
        },
        {
          name: 'Чтение документации на английском языке',
          children: [],
        },
      ],
    },
    {
      name: 'Владеть',
      children: [
        {
          name: 'Работа в команде',
          children: [
            {
              name: 'agile',
              weight: 0.005,
            },
            {
              name: 'скрам',
              weight: 0.003,
            },
            {
              name: 'компания',
            },
            {
              name: 'проект',
            },
            {
              name: 'продуктовый',
            },
          ],
        },
        {
          name: 'Системное мышление',
          children: [],
        },
        {
          name: 'Система контроля версий Git',
          children: [
            {
              name: 'git',
              weight: 0.1435,
            },
            {
              name: 'ветка',
              weight: 0.0444,
            },
            {
              name: 'master',
              weight: 0.0287,
            },
            {
              name: 'commit',
              weight: 0.0166,
            },
            {
              name: 'head',
              weight: 0.0132,
            },
            {
              name: 'origin',
              weight: 0.013,
            },
          ],
        },
      ],
    },
  ],
};

var i = 0,
  duration = 750,
  rectW = 225,
  rectH = 35;

var tree = d3.layout.tree().nodeSize([235, 45]);
var diagonal = d3.svg.diagonal().projection(function (d) {
  return [d.x + rectW / 2, d.y + rectH / 2];
});

var svg = d3
  .select('#container')
  .append('svg')
  .attr('width', '100%')
  .attr('height', '100%')
  .call((zm = d3.behavior.zoom().scaleExtent([1, 3]).on('zoom', redraw)))
  .append('g')
  .attr('transform', 'translate(' + 350 + ',' + 20 + ')');

zm.translate([350, 20]);

root.x0 = 0;
root.y0 = height / 2;

function collapse(d) {
  if (d.children) {
    d._children = d.children;
    d._children.forEach(collapse);
    d.children = null;
  }
}

root.children.forEach(collapse);
update(root);

d3.select('#container').style('height', '800px');

function update(source) {
  var nodes = tree.nodes(root).reverse(),
    links = tree.links(nodes);

  nodes.forEach(function (d) {
    d.y = d.depth * 180;
  });

  var node = svg.selectAll('g.node').data(nodes, function (d) {
    return d.id || (d.id = ++i);
  });

  var nodeEnter = node
    .enter()
    .append('g')
    .attr('class', 'node')
    .attr('transform', function (d) {
      return 'translate(' + source.x0 + ',' + source.y0 + ')';
    })
    .on('click', click);

  nodeEnter
    .append('rect')
    .attr('width', rectW)
    .attr('height', rectH)
    .attr('stroke', 'blue')
    .attr('stroke-width', 1)
    .style('fill', function (d) {
      return d._children ? 'lightsteelblue' : '#ffffff';
    });

  nodeEnter
    .append('text')
    .attr('x', rectW / 2)
    .attr('y', rectH / 2)
    .attr('dy', '.35em')
    .attr('text-anchor', 'middle')
    .text(function (d) {
      weight = d.weight ? `(${d.weight})` : '';
      return `${d.name} ${weight}`;
    });

  var nodeUpdate = node
    .transition()
    .duration(duration)
    .attr('transform', function (d) {
      return 'translate(' + d.x + ',' + d.y + ')';
    });

  nodeUpdate
    .select('rect')
    .attr('width', rectW)
    .attr('height', rectH)
    .attr('stroke', 'black')
    .attr('stroke-width', 1)
    .style('fill', function (d) {
      return d._children ? 'lightsteelblue' : '#fff';
    });

  nodeUpdate.select('text').style('fill-opacity', 1);

  var nodeExit = node
    .exit()
    .transition()
    .duration(duration)
    .attr('transform', function (d) {
      return 'translate(' + source.x + ',' + source.y + ')';
    })
    .remove();

  nodeExit.select('rect').attr('width', rectW).attr('height', rectH).attr('stroke', 'black').attr('stroke-width', 1);

  nodeExit.select('text');

  var link = svg.selectAll('path.link').data(links, function (d) {
    return d.target.id;
  });

  link
    .enter()
    .insert('path', 'g')
    .attr('class', 'link')
    .attr('x', rectW / 2)
    .attr('y', rectH / 2)
    .attr('d', function (d) {
      var o = {
        x: source.x0,
        y: source.y0,
      };
      return diagonal({
        source: o,
        target: o,
      });
    });

  link.transition().duration(duration).attr('d', diagonal);

  link
    .exit()
    .transition()
    .duration(duration)
    .attr('d', function (d) {
      var o = {
        x: source.x,
        y: source.y,
      };
      return diagonal({
        source: o,
        target: o,
      });
    })
    .remove();

  nodes.forEach(function (d) {
    d.x0 = d.x;
    d.y0 = d.y;
  });
}

function click(d) {
  if (d.children) {
    d._children = d.children;
    d.children = null;
  } else {
    d.children = d._children;
    d._children = null;
  }
  update(d);
}

function redraw() {
  svg.attr('transform', 'translate(' + d3.event.translate + ')' + ' scale(' + d3.event.scale + ')');
}
