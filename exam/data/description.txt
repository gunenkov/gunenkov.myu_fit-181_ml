1) cdc_report_dt -- Date cdc reported
2) pos_spec_dt -- Date of first positive specimen collection (MM/DD/YYYY)
3) onset_dt -- What was the onset date?
4) current_status -- What is the current status of this person?
5) sex -- Gender
6) age_group -- Age group categories
7) Race and ethnicity -- Case Demographic
8) hosp_yn -- Was the patient hospitalized?
9) icu_yn -- Was the patient admitted to an intensive care unit (ICU)?
10 death_yn -- Did the patient die as a result of this illness?
11) medcond_yn -- Did they have any underlying medical conditions and/or risk behaviors?