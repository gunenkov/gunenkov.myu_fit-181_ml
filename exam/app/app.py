import base64
import os

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sbn
import streamlit as st
from catboost import CatBoostClassifier


def show_title_with_subtitle():
    # Заголовок и подзаголовок
    st.title("COVID-19")
    st.write("# Прогнозирование летального исхода")


def show_info_page():
    show_title_with_subtitle()
    st.image("https://journals.physiology.org/pb-assets/images/custom-pages/Disease-Day-Collections/COVID19/COVID19-2.jpg",
             use_column_width=True)
    st.write("## Точность получаемых прогнозов: более 99.9%!")
    st.write(
        "Предлагаемая модель обучалась на большом количестве предварительно обработанных данных. Тщательным образом "
        "подобраны гиперпараметры модели, что гарантирует высокое качество классификации")
    st.write("## Учет множества обстоятельств")
    st.write(
        "Данные, на которых обучалась модель, представляют собой признаковое описание, достаточное для того, чтобы получить качественный "
        "прогноз летального исхода от болезни COVID-19")
    st.write("## Отечественный градиентный бустинг - гарант качества")
    st.write(
        "В результате анализа метрик качества нескольких моделей выбрана модель CatBoostClassifier, разработанная "
        "Яндексом")
    st.write("Подробнее о CatBoost: https://catboost.ai")


def prepare_data(data):
    data = pd.get_dummies(columns=["age_group", "current_status", "sex",
                                   "Race and ethnicity (combined)", "hosp_yn",
                                   "icu_yn", "medcond_yn"],
                          prefix={"age_group": "age",
                                  "current_status": "status",
                                  "sex": "sex",
                                  "Race and ethnicity (combined)": "race",
                                  "hosp_yn": "hosp_yn",
                                  "icu_yn": "icu_yn",
                                  "medcond_yn": "medcond_yn"},
                          data=data)

    example_data = pd.read_csv(os.path.join(os.path.dirname(__file__), "data", "example_data.csv"))
    new_data = pd.DataFrame(columns=example_data.columns)
    new_data.drop(["Unnamed: 0"], axis=1, inplace=True)
    new_data = new_data.append(data)
    st.write("### Предобработанные данные:")
    st.write(new_data.fillna(0))
    return new_data.fillna(0)


def input_manual():
    cdc_report_month = st.selectbox("Report Month:", (range(1, 13)))
    cdc_report_day = st.selectbox("Report Day:", (range(1, 32)))
    current_status = st.selectbox("Current Status:", ("Laboratory-confirmed case", "Probable Case"))
    sex = st.selectbox("Sex:", ("Male", "Female", "Other"))
    age_group = st.selectbox("Age Group:", ("0 - 9 Years", "10 - 19 Years", "20 - 29 Years", "30 - 39 Years",
                                            "40 - 49 Years", "50 - 59 Years", "60 - 69 Years", "70 - 79 Years",
                                            "80+ Years", "Unknown"))
    race = st.selectbox("Race and ethnicity (combined):", ("Black, Non-Hispanic", "White, Non-Hispanic",
                                                           "Asian, Non-Hispanic", "American Indian/Alaska Native, Non-Hispanic",
                                                           "Multiple/Other, Non-Hispanic", "Native Hawaiian/Other Pacific Islander, Non-Hispanic",
                                                           "Hispanic/Latino", "Missing", "Unknown"))
    hosp_yn = st.selectbox("Hosp YN:", ("Yes", "No", "Missing", "Unknown"))
    icu_yn = st.selectbox("Icu YN:", ("Yes", "No", "Missing", "Unknown"))
    medcond_yn = st.selectbox("MedCond YN:", ("Yes", "No", "Missing", "Unknown"))
    predict_button = st.button("Получить прогноз")
    if predict_button:
        data = pd.DataFrame(columns=["cdc_report_month", "cdc_report_day", "age_group", "current_status", "sex",  "Race and ethnicity (combined)", "hosp_yn",
                                     "icu_yn", "medcond_yn"])
        data.loc[0] = [cdc_report_month, cdc_report_day, age_group, current_status, sex,  race, hosp_yn, icu_yn, medcond_yn]
        make_predictions(get_model(), prepare_data(data))


def input_file():
    st.write("Файл для примера:")
    with open(os.path.join(os.path.dirname(__file__), "data", "example_data.csv"), "rb") as f:
        file = f.read()
        example_data = base64.b64encode(file).decode()
    st.markdown(f'<a href="data:file/csv;base64,{example_data}" download="example_data.csv">Скачать csv</a>', unsafe_allow_html=True)
    file = st.file_uploader(label="Выберите csv файл с предобработанными данными для прогнозирования стоимости",
                            type=["csv"],
                            accept_multiple_files=False)
    if file is not None:
        test_data = pd.read_csv(file)
        st.write("### Загруженные данные")
        st.write(test_data)
        make_predictions(get_model(), test_data)


def show_predictions_page():
    input_method = st.radio("Выберите способ ввода данных:", ("Ввести вручную", "Загрузить файл (данные предобработаны)"))

    if input_method == "Ввести вручную":
        input_manual()
    else:
        input_file()


def get_model():
    return CatBoostClassifier().load_model(os.path.join(os.path.dirname(__file__), "models", "cb"))


def make_predictions(model, X):
    st.write("### Предсказанные значения")
    pred = pd.DataFrame(columns=["Prediction"], data=model.predict(X))
    st.write(pred)
    st.write("### Гистограмма распределения предсказаний")
    plot_hist(pred["Prediction"])
    result_b64 = base64.b64encode(pred.to_csv(index=False).encode()).decode()
    st.markdown(f'<a href="data:file/csv;base64,{result_b64}" download="predictions.csv">Скачать результат в csv</a>',
                unsafe_allow_html=True)


def plot_hist(data):
    fig = plt.figure()
    sbn.histplot(data, legend=False)
    st.pyplot(fig)


def select_page():
    return st.sidebar.radio("Выберите страницу", ("Информация", "Прогнозирование"))


st.set_page_config(page_title="COVID-19. Прогнозирование летального исхода",
                   page_icon="https://img.icons8.com/plasticine/2x/coronavirus.png")

# Стиль для скрытия со страницы меню и футера streamlit
hide_st_style = """
            <style>
            #MainMenu {visibility: hidden;}
            footer {visibility: hidden;}
            </style>
            """
st.markdown(hide_st_style, unsafe_allow_html=True)

# размещение элементов на странице
st.sidebar.title("Меню")
page = select_page()
st.sidebar.write("© Mikhail Gunenkov 2021")
st.sidebar.write("https://vk.com/m_gunenkov")
st.sidebar.write("https://gitlab.com/gunenkov")

if page == "Информация":
    show_info_page()
else:
    show_predictions_page()
