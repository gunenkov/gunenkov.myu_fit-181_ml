import numpy as np


# P(some_class | X) = P(some_class) * P(X1 | some_class) * ... * P(Xn | some_class)
# P(some_class | X) - posterior
# P(some_class) - prior
class MyGaussianNB:
    def calc_means_and_vars(self, X, y):
        self.means = X.groupby(y).apply(np.mean).to_numpy()
        self.variances = X.groupby(y).apply(np.var).to_numpy()
        return self.means, self.variances

    def calculate_probability(self, class_idx, x):
        mean = self.means[class_idx]
        variance = self.variances[class_idx]
        return np.exp((-1 / 2) * ((x - mean) ** 2) / (2 * variance)) / np.sqrt(2 * np.pi * variance)

    def calculate_priors(self, X, y):
        self.priors = (X.groupby(y).apply(lambda x: len(x)) / self.count_of_objects).to_numpy()
        return self.priors

    def calculate_max_posterior(self, x):
        posteriors = []
        for i in range(self.count_of_classes):
            posterior = self.priors[i] * np.prod(self.calculate_probability(i, x))
            posteriors.append(posterior)
        return self.classes[np.argmax(posteriors)]

    def fit(self, X, y):
        self.classes = np.unique(y)
        self.count_of_classes = len(self.classes)
        self.count_of_objects = X.shape[0]
        self.count_of_features = X.shape[1]
        self.calc_means_and_vars(X, y)
        self.calculate_priors(X, y)
        return self

    def predict(self, X):
        return [self.calculate_max_posterior(x) for x in X.to_numpy()]
